import discord
import random, dotenv, os
from datetime import datetime
from os import system

user = None
client = discord.Client()

def log(r):
    global user
    with open('attempts.log', 'a+') as logs:
        log_line = str(datetime.now()) + ' : ' + str(user) + '\n'
        print(log_line)
        logs.write(log_line)

def get_file():
    try:
        # grab the meta file first then remove it ourselves
        if 'thread_link' not in os.listdir('output'):
            link = None
        else:
            with open('output/thread_link', 'r') as tl:
                link = tl.read().strip('\n')
            os.remove('output/thread_link')
        file_ = 'output/' + os.listdir('output')[0]
        return {'link': link, 'f':file_}

    except IndexError:
        print(f'Directory content: {os.listdir("output")}')
        return {'link': None, 'f': None}


async def attempt_dm(channel):
    global user
    valid = [] # list of eligible people for the potential to receive hentai
    if len(channel.members) < int(os.getenv('CHAN_MIN')):
        if int(os.getenv('BOT_DEV')):
            pass
        else:
            return False

    # pillow chat role id: 128762703886942208 
    for member in channel.members:
        roles = [r.id for r in member.roles]
        if 128762703886942208 in roles:
            valid.append(member)

    # shut up that one err we randomly get sometimes
    if len(valid) == 0:
        return False

    try:
        recipient = random.choice(valid)
        if recipient.dm_channel is None:
            await recipient.create_dm()

        print(recipient)
        data = get_file()
        if int(os.getenv('BOT_DEV')):
            print(f'Simulated send: File: {data["f"]}\tLink: {data["link"]}')
            return True

        try:
            f = discord.File(data['f'])
            await recipient.dm_channel.send(data['link'], file=f)
        except Exception:
            '''
            prolly because we tried to dm someone that has us blocked but idc just move on
            '''
            return 'Blocked user'

        user = str(recipient)
        return True
    except IndexError:
        return False

    return False


@client.event
async def on_ready():
    server = client.guilds[int(os.getenv('BOT_GUILD'))]
    for chan in server.voice_channels:
        # Check to see what permissions the users have
        result = await attempt_dm(chan)
            
    log(result)
    await client.logout()

if __name__ == '__main__':

    dotenv.load_dotenv()
    if int(os.getenv('BOT_DEV')):
        print('Dev test run')

    # dice roll if we send noods or not random(((safe))) image
    client.run(os.getenv('BOT_KEY'))

