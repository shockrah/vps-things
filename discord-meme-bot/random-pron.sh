#!/bin/bash

log() {
	echo `date` >> run.log
	[ $1 = 'fail' ] && echo `date` 4CHAN API BUSTED AGAIN >> attempts.log; exit 1
}

mkdir -p output/
swap_=output/swap

python3 rng.py > $swap_
img="`head -1 $swap_`"
thread="`tail -1 $swap_`"
rm -f $swap_
echo $thread > output/thread_link

if [ -z "$img" ];then
	log fail
else
	wget --quiet "$img" -O "output/`basename $img`"
	
	python3 bot.py
	rm -rf output/
fi

