#!/bin/sh


vbox() {
	wget -O - https://db.debian.org/fetchkey.cgi?fingerprint=FEDEC1CB337BCF509F43C2243914B532F4DFBE99 | apt-key add
	echo 'deb https://people.debian.org/~lucas/virtualbox-buster/ ./' > /etc/apt/sources.list.d/virtualbox-unofficial.list
	apt update
	apt install virtualbox

}

basic_deps() {
	cat > /etc/apt/sources.list <<EOF
# Buster > Stretch
deb http://deb.debian.org/debian buster main
deb-src http://deb.debian.org/debian buster main

deb http://deb.debian.org/debian-security/ buster/updates main
deb-src http://deb.debian.org/debian-security/ buster/updates main

deb http://deb.debian.org/debian buster-updates main
deb-src http://deb.debian.org/debian buster-updates main
EOF
	apt update
	apt upgrade -y
	apt-get dist-upgrade
	apt install python3
	python3 -m pip install virtualenv

	apt install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev wget -y
	# this is for some deps we'll need in the virtualenv setup
	apt install libffi-dev libnacl-dev python3-dev -y
	apt install libcurl4-gnutls-dev librtmp-dev software-properties-common -y
	apt install reportbug pkg-config libcairo2-dev -y
	vbox
}

hentai_bot() {
	cd discord-meme-bot/
	pip3 install -r requirements.txt
	# TODO: setup the cron job for that thing now
	#echo "*/15 * * * * $CRON_SCRIPT_FILE" >> $CRON_TAB_FILE
	#echo '# ignore this line' >> $CRON_TAB_FILE
}

cache() {
	echo $1 >> install.cache
}

while getopts ':h' OPT;do
	case $OPT in
		h) hentai_bot;;
	esac
done
