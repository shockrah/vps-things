# This bot will serve as a simple radio bot
from os import getenv
from dotenv import load_dotenv
from discord import Client
from discord.ext import commands
import discord

bot = commands.Bot(command_prefix='.')

@bot.event
async def on_ready():
    print(f'logged in as {bot.user.name}')

if __name__ == '__main__':
    load_dotenv()
    print('loading radio cog')
    bot.load_extension('radio')
    bot.run(getenv('API_KEY'), bot=True, reconnect=True)

