from discord.ext import commands

class RadioCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot


    @commands.group(pass_context=True, name='radio')
    async def radio(self, ctx):
        if ctx.invoked_subcommand is None:
            link = ctx.message.content[len('.radio'):].strip('\n\t ')
            await ctx.send('joining')
            vc = await ctx.message.author.voice.channel.connect()



    @radio.command(name='shuffle')
    async def shuffle(self, ctx, link:str):
        await ctx.send(f'shuffling: {link}')


    @radio.command(name='pause')
    async def pause(self, ctx):
        pass

    @radio.command(name='play')
    async def play(self, ctx):
        pass



def setup(bot):
    bot.add_cog(RadioCog(bot))
